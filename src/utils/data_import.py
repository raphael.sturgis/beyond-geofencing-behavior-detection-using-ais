import pandas as pd
from skais.ais.ais_points import AISPoints
from skais.process.ais_operations import get_trajectories

from src.utils.experiment_tools import clean_and_interpolate_trajectories


def load_dataset_from_csv(csv_path, indices=None):
    df = pd.read_csv(csv_path)
    df = df[df.label != -1]

    if not indices:
        mmsis = df.mmsi.unique()
        for mmsi in [mmsis[i] for i in indices]:
            df = df[df.mmsi != mmsi]

    points = AISPoints(df)
    points.remove_outliers(['sog'])
    points.df['ts_sec'] = points.df['ts'].astype('int64') // 1e9
    trajectories = get_trajectories(points)

    return clean_and_interpolate_trajectories(trajectories, 60, 300, cut_off=50)