from random import random

import numpy as np
import pandas as pd
from skais.learn.data_augmentation import shift_positions
from sklearn import metrics
from tqdm import tqdm


def generate_between(a, b):
    return a + (b - a) * random()


def shift(trajectories, s=1):
    result = []
    if s == 0:
        return trajectories
    for _ in range(s):
        for trajectory in trajectories:
            result.append(
                shift_positions(
                    trajectory,
                    generate_between(-np.pi, np.pi),
                    generate_between(-np.pi, np.pi),
                    generate_between(-np.pi, np.pi)
                )
            )
    return result

def unison_shuffled_copies(a, b):
    c = list(zip(a, b))

    random.shuffle(c)

    return zip(*c)

def evaluate_model(model, x_test, y_test):
    y_predict = model.predict(x_test)
    y_true = y_test

    acc = metrics.accuracy_score(y_true, y_predict)
    f1 = metrics.f1_score(y_true, y_predict, average='weighted')
    c_mat = metrics.confusion_matrix(y_true, y_predict)
    return acc, f1, c_mat


def get_transition_indexes(trajectories):
    indexes = []
    for trajectory in tqdm(trajectories, leave=False, colour='yellow'):
        df = trajectory.df[['label']].rolling(2).apply(lambda x: x[0] != x[-1], raw=True).dropna().astype(int)
        index = df[df['label'] != 0].index
        indexes.append(index)
    return indexes

def get_point_near_transition(trajectories, radius, transitions=None, keep_duplicates=False):
    if transitions is None:
        transitions = get_transition_indexes(trajectories)
    result = []
    negative = []
    for indexes, trajectory in tqdm(zip(transitions, trajectories), total=len(trajectories), leave=False, colour='green'):
        nb_data_points = len(trajectory.df.index)
        segments = []
        for index in indexes:
            position = trajectory.df.index.get_loc(index)
            segments.append(trajectory.df.iloc[max(0, position - radius):min(position + radius, nb_data_points)])

        if len(segments) > 0:
            trajectory_segments = pd.concat(segments)
            if not keep_duplicates:
                trajectory_segments = trajectory_segments[~trajectory_segments.index.duplicated(keep='first')]

            negative.append(trajectory.df.drop(trajectory_segments.index))
            result.append(trajectory_segments)
        else:
            negative.append(trajectory.df)

    return pd.concat(result), pd.concat(negative)

