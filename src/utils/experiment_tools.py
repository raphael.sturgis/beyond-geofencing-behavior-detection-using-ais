import numpy as np
import tqdm as tqdm
from skais.ais.ais_points import AISPoints

from src.utils.features import compute_selected_features


def clean_and_interpolate_trajectories(trajectories, interpolation_time=60, time_gap=3600, cut_off=-1):
    trajectories_split = []
    for trajectory in tqdm.tqdm(trajectories, desc='splitting trajectories', position=0):
        trajectories_split += trajectory.split_trajectory(time_gap=time_gap, interpolation=interpolation_time)

    result = []
    for trajectory in tqdm.tqdm(trajectories_split, desc='cleaning trajectories', position=0):
        if len(trajectory.df.index) > cut_off:
            trajectory.remove_outliers(['sog'])
            result.append(trajectory)

    return result


def match_prediction_to_trajectories(trajectories, prediction, name='prediction', tqdm_disable=True, tqdm_position=0, tqdm_colour='magenta'):
    index = 0
    for trajectory in tqdm.tqdm(trajectories, disable=tqdm_disable, colour=tqdm_colour, position=tqdm_position, leave=False):
        trajectory.df[name] = prediction[index:index + len(trajectory.df.index)]
        index += len(trajectory.df.index)

    return trajectories

def get_normalisation_factors(trajectories):
    points = AISPoints.fuse(trajectories)
    return points.normalize(
        third_quartile_features=['sog'],
        divide_by_value=[('latitude', 90), ('longitude', 180), ('heading', 180), ('cog', 180)]
    )


def compute_features_on_trajectories_interpolated(trajectories, window_size, tqdm_position=0):
    trajectories_with_computed_features = []
    for trajectory in tqdm.tqdm(trajectories, leave=False, position=tqdm_position):
        if len(trajectory.df.index) > 12:
            compute_selected_features(trajectory, window_size)
            trajectories_with_computed_features.append(trajectory)
    return trajectories_with_computed_features
#
#
# def make_feature_vectors(trajectories, features=None,
#                          label_field='label', nb_classes=2, sliding_window_gap=10, length_list=15, verbose=0):
#     if features is None:
#         features = ['rot', 'sog', 'd_sog', 'd_cog', 'd_heading', 'd_rot']
#     x = []
#     y = []
#     features_trajectories = []
#     label_trajectories = []
#     zero = [0 for _ in range(nb_classes)]
#
#     iterator = trajectories
#     if verbose > 0:
#         iterator = tqdm.tqdm(trajectories)
#     for trajectory in iterator:
#         if len(trajectory.df.index) > length_list:
#             trajectory.df['ts'] = trajectory.df.index
#             windows = trajectory.sliding_window(length_list, offset=sliding_window_gap,
#                                                 fields=features + [label_field])
#
#             features_trajectories.append(trajectory.to_numpy(fields=features))
#             label_trajectories.append(trajectory.to_numpy(fields=[label_field]).astype(int))
#
#             if length_list > 0:
#                 for window in windows:
#                     label = zero.copy()
#                     label[int(window[length_list // 2, -1])] = 1
#                     x.append(window[:, :-1])
#                     y.append(label)
#
#     return features_trajectories, label_trajectories, x, y
#
#
def normalize_set_of_trajectories(trajectories, dictionary):
    for t in trajectories:
        t.normalize(normalization_dict=dictionary)
#
#
# def split_trajectory_in_window(trajectory, size=10, offset=1):
#     trajectories = []
#     for i in range(0, len(trajectory.df.index) - size, offset):
#         df = trajectory.df.iloc[i:i + size]
#         trajectories.append(AISTrajectory(df))
#     return trajectories
#
# def quick_split_trajectory_in_window(trajectory, size=10, offset=1, features=None):
#     trajectories = []
#
#     if features is None:
#         x = trajectory.df[['latitude', 'longitude']].to_numpy()
#     else:
#         x = trajectory.df[['latitude', 'longitude'] + features].to_numpy()
#
#     for i in range(0, len(trajectory.df.index) - size, offset):
#         v = x[i:i + size]
#         trajectories.append(v)
#     return trajectories
#
# def split_trajectory_in_time_window(trajectory, time_window=10, offset=1):
#     trajectories = []
#     label = []
#     min_ts = trajectory.df.ts_sec.min()
#     for i in range(0, len(trajectory.df.index), offset):
#         time = trajectory.df.iloc[i].ts_sec
#         if time - time_window >= min_ts:
#             df = trajectory.df[
#                 (trajectory.df['ts_sec'] > time - time_window) & (trajectory.df['ts_sec'] < time + time_window)]
#             trajectories.append(AISTrajectory(df))
#             label.append(trajectory.df.iloc[i].label)
#     return trajectories, label
#
#
# def split_trajectory_in_window_single_label(trajectory, size=10, offset=1):
#     trajectories = []
#     labels = []
#     for i in range(0, len(trajectory.df.index) - size, offset):
#         df = trajectory.df.iloc[i:i + size]
#         if len(df['label'].unique()) == 1:
#             trajectories.append(AISTrajectory(df))
#             labels.append(df['label'].iloc[0])
#     return trajectories, labels
#
#
# def split_in_time_windows(x, y, size=10, offset=1, label_position=5):
#     assert (len(x) == len(y))
#     assert offset > 0
#
#     x_windows = []
#     y_windows = []
#     for tx, ty in zip(x, y):
#         assert len(tx) == len(ty)
#         index = 0
#         while index + size < len(tx):
#             x_windows.append(tx[index:index + size])
#             y_windows.append(ty[index + label_position])
#             index += offset
#
#     return x_windows, y_windows
#
# def all_equal(iterator):
#     iterator = iter(iterator)
#     try:
#         first = next(iterator)
#     except StopIteration:
#         return True
#     return all(first == x for x in iterator)
#
# def split_in_time_windows_single_label(x, y, size=10, offset=1):
#     assert (len(x) == len(y))
#     assert offset > 0
#
#     x_windows = []
#     y_windows = []
#     for tx, ty in zip(x, y):
#         assert len(tx) == len(ty)
#         index = 0
#         while index + size < len(tx):
#             if all_equal(ty[index:index + size]):
#                 x_windows.append(tx[index:index + size])
#                 y_windows.append(ty[index])
#                 index += offset
#             else:
#                 index += 1
#
#     return x_windows, y_windows
#
def convert_to_vectors(trajectories, features):
    X = []
    Y = []
    for trajectory in trajectories:
        X.append(trajectory.df[features].to_numpy())
        y = trajectory.df['label'].to_numpy()
        np.zeros((y.size, 4))
        y_one_hot = np.zeros((y.size, 4))
        y_one_hot[np.arange(y.size), y] = 1
        Y.append(y_one_hot)
    return X, Y


def convert_to_vectors_not_one_hot(trajectories, features, tqdm_disable=False, tqdm_position=1, tqdm_leave=False):
    X = []
    Y = []
    for trajectory in tqdm.tqdm(trajectories, position=tqdm_position, leave=tqdm_leave, disable=tqdm_disable):
        X.append(trajectory.df[features].to_numpy())
        y = trajectory.df['label'].to_numpy()
        Y.append(y)
    return X, Y
#
#
# def get_scores(model, x_test, y_test):
#     predictions = []
#     for x in tqdm.tqdm(x_test):
#         predictions.append(model.predict(np.array([x])))
#
#     y_predict = np.concatenate(predictions, axis=1)[0]
#     y_true = np.concatenate(y_test)
#
#     return get_scores_label(np.argmax(y_true, axis=1), np.argmax(y_predict, axis=1))
#
#
# def get_scores_label(y_true, y_predict):
#     acc = metrics.accuracy_score(y_true, y_predict)
#     precision = metrics.precision_score(y_true, y_predict,
#                                         average='weighted')
#     recall = metrics.recall_score(y_true, y_predict, average='weighted')
#     f1 = metrics.f1_score(y_true, y_predict, average='weighted')
#
#     confusion_matrix = metrics.confusion_matrix(y_true, y_predict)
#
#     return acc, precision, recall, f1, confusion_matrix
#
#
# def get_scores_per_label(y_true, y_predict):
#     confusion_matrix = metrics.confusion_matrix(y_true, y_predict)
#
#     return confusion_matrix.diagonal() / confusion_matrix.sum(axis=1)
#
#
def split_to_size_and_complete(x, y, size=100, value=np.nan):
    result = []
    labels = []
    for i in range(0, len(x), size):
        result.append(x[i:i + size])
        labels.append(y[i:i + size])
    if 100 > len(result[-1]):
        result[-1] = np.concatenate((result[-1], np.full((100 - len(result[-1]), result[-1].shape[1]), value)))
        labels[-1] = np.concatenate((labels[-1], np.full((100 - len(labels[-1]), labels[-1].shape[1]), 0)))
    return result, labels


def split_to_size_list(X, Y, size=100, value=np.nan):
    x_n = []
    y_n = []
    for x, y in zip(X, Y):
        x, y = split_to_size_and_complete(x, y, size, value)
        x_n += x
        y_n += y
    return x_n, y_n
#
#
# def evaluate_model_one_hot(model, x_test, y_test):
#     predictions = []
#     for x in x_test:
#         predictions.append(model.predict(np.array([x])))
#
#     y_predict = np.argmax(np.concatenate(predictions, axis=1)[0], axis=1)
#     y_true = np.argmax(np.concatenate(y_test), axis=1)
#
#     acc = metrics.accuracy_score(y_true, y_predict)
#     precision = metrics.precision_score(y_true, y_predict, average='weighted')
#     recall = metrics.recall_score(y_true, y_predict, average='weighted')
#     f1 = metrics.f1_score(y_true, y_predict, average='weighted')
#
#     return acc, precision, recall, f1
#
#
# def evaluate_model_one_hot_masked(model, x_test, y_test, mask_value=99999.99, batch_size=16, verbose=0):
#     predictions = model.predict(np.array(x_test), batch_size=batch_size, verbose=verbose)
#     M = np.ma.masked_equal(np.concatenate(x_test)[:,0], mask_value)
#     y_predict = np.argmax(np.concatenate(predictions, axis=0), axis=1)[~M.mask.reshape(-1)]
#
#     y_true = np.argmax(np.concatenate(y_test, axis=0), axis=1)[~M.mask.reshape(-1)]
#
#     acc = metrics.accuracy_score(y_true, y_predict)
#     precision = metrics.precision_score(y_true, y_predict, average='weighted')
#     recall = metrics.recall_score(y_true, y_predict, average='weighted')
#     f1 = metrics.f1_score(y_true, y_predict, average='weighted')
#
#     return acc, precision, recall, f1
#
# def split_on_time(trajectories, time_gap, cut_off=-1):
#     trajectories_split = []
#     for trajectory in tqdm.tqdm(trajectories, desc='splitting trajectories', position=0):
#         trajectories_split += trajectory.split_trajectory(time_gap=time_gap, interpolation=None)
#
#     result = []
#     for trajectory in tqdm.tqdm(trajectories_split, desc='cleaning trajectories', position=0):
#         if len(trajectory.df.index) > cut_off:
#             trajectory.remove_outliers(['sog'])
#             result.append(trajectory)
#
#     return result