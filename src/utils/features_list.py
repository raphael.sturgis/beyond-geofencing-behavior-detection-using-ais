FEATURES_LIST = ['sog', 'drift', 'delta_sog', 'delta_cog', 'delta_heading', 'std_cog',
                 'std_hdg', 'std_delta_cog', 'std_delta_heading', 'std_pos', 'std_sog']

FEATURES_COMBINATIONS = [['sog'], ['cog'], ['heading'], ['latitude', 'longitude'],
                         ['cog', 'heading', 'longitude', 'latitude'],
                         ['sog', 'cog', 'heading', 'longitude', 'latitude']]