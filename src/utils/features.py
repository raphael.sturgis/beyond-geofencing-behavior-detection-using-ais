from copy import deepcopy

import numpy as np
import skais.utils.geography
from skais.process.basic_features_operations import time_derivative, angular_std, angular_time_derivative, angular_mean
from tqdm import tqdm

FEATURES_LIST = ['sog', 'drift', 'delta_sog', 'delta_cog', 'delta_heading', 'std_cog',
                 'std_hdg', 'std_delta_cog', 'std_delta_heading', 'std_pos', 'std_sog']


def position_std(data):
    long_mean = np.degrees(angular_mean(np.radians(data[:, 0])))
    lat_mean = np.degrees(angular_mean(np.radians(data[:, 1])))

    distance_sum = 0
    for long, lat in data:
        distance_sum += skais.utils.geography.great_circle.py_func(lat_mean, lat, long_mean, long) ** 2
    return np.sqrt(distance_sum / len(data))


def compute_selected_features_on_trajectories(trajectories, radius, min_size=12, tqdm_disable=False):
    out = []
    for trajectory in tqdm(deepcopy(trajectories), disable=tqdm_disable):
        if len(trajectory.df.index) > min_size:
            compute_selected_features(trajectory, radius)
            out.append(trajectory)
    return out


def compute_selected_features_on_trajectories_multi_time_window(trajectories, radii, min_size=12):
    out = []
    features = None
    for trajectory in tqdm(deepcopy(trajectories), desc="computing features"):
        trajectory.remove_outliers(['sog'])
        trajectory.clean_angles()
        if len(trajectory.df.index) > min_size:
            features = compute_selected_features_multi_time_window(trajectory, radii)
            out.append(trajectory)
    return out, features


def compute_selected_features(trajectory, time_window_radius):
    trajectory.clean_angles()
    trajectory.compute_drift()

    trajectory.apply_func_on_time_sequence(time_derivative, 'sog', new_column='delta_sog')

    trajectory.apply_func_on_points(np.radians, 'cog', new_column='cog_r')
    trajectory.apply_func_on_points(np.radians, 'heading', new_column='heading_r')
    trajectory.apply_func_on_time_sequence(angular_time_derivative, 'cog_r',
                                           new_column='delta_cog')
    trajectory.apply_func_on_points(np.abs, 'delta_cog')  # curvature cog:
    trajectory.apply_func_on_time_sequence(angular_time_derivative, 'heading_r',
                                           new_column='delta_heading')
    trajectory.apply_func_on_points(np.abs, 'delta_heading')  # curvature cog

    trajectory.apply_func_on_time_window(angular_std, time_window_radius, 'cog_r', 'std_cog', on_edge='ignore')
    trajectory.apply_func_on_time_window(angular_std, time_window_radius, 'heading_r', 'std_hdg', on_edge='ignore')
    trajectory.apply_func_on_time_window(angular_std, time_window_radius, 'delta_cog', 'std_delta_cog',
                                         on_edge='ignore')
    trajectory.apply_func_on_time_window(angular_std, time_window_radius, 'delta_heading', 'std_delta_heading',
                                         on_edge='ignore')

    trajectory.apply_func_on_time_window(position_std, time_window_radius, ['longitude', 'latitude'], 'std_pos',
                                         on_edge='ignore')

    trajectory.apply_func_on_time_window(np.std, time_window_radius, 'sog', 'std_sog', on_edge='ignore')


def compute_selected_features_multi_time_window(trajectory, time_window_radii):
    trajectory.compute_drift()
    trajectory.apply_func_on_time_sequence(time_derivative, 'sog', new_column='delta_sog')
    trajectory.apply_func_on_points(np.radians, 'cog', new_column='cog_r')
    trajectory.apply_func_on_points(np.radians, 'heading', new_column='heading_r')
    trajectory.apply_func_on_time_sequence(angular_time_derivative, 'cog_r',
                                           new_column='delta_cog')
    trajectory.apply_func_on_points(np.abs, 'delta_cog')  # curvature cog:
    trajectory.apply_func_on_time_sequence(angular_time_derivative, 'heading_r',
                                           new_column='delta_heading')
    trajectory.apply_func_on_points(np.abs, 'delta_heading')  # curvature cog

    features_list = ['sog', 'drift', 'delta_sog', 'delta_cog', 'delta_heading']
    for time_window_radius in time_window_radii:
        trajectory.apply_func_on_time_window(angular_std, time_window_radius, 'cog_r', f'std_cog_{time_window_radius}',
                                             on_edge='ignore')
        trajectory.apply_func_on_time_window(angular_std, time_window_radius, 'heading_r',
                                             f'std_hdg_{time_window_radius}', on_edge='ignore')
        trajectory.apply_func_on_time_window(angular_std, time_window_radius, 'delta_cog',
                                             f'std_delta_cog_{time_window_radius}',
                                             on_edge='ignore')
        trajectory.apply_func_on_time_window(angular_std, time_window_radius, 'delta_heading',
                                             f'std_delta_heading_{time_window_radius}',
                                             on_edge='ignore')

        trajectory.apply_func_on_time_window(position_std, time_window_radius, ['longitude', 'latitude'],
                                             f'std_pos_{time_window_radius}',
                                             on_edge='ignore')

        trajectory.apply_func_on_time_window(np.std, time_window_radius, 'sog', f'std_sog_{time_window_radius}',
                                             on_edge='ignore')

        features_list += [f'std_cog_{time_window_radius}', f'std_hdg_{time_window_radius}',
                          f'std_delta_cog_{time_window_radius}', f'std_delta_heading_{time_window_radius}',
                          f'std_pos_{time_window_radius}', f'std_sog_{time_window_radius}']
    return features_list
