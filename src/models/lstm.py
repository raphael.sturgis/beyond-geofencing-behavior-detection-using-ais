import os

import numpy as np
from dotenv import load_dotenv
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.models import load_model

from src.utils.data_import import load_dataset_from_csv
from src.utils.experiment_tools import get_normalisation_factors, normalize_set_of_trajectories
from src.utils.features_list import FEATURES_COMBINATIONS
from src.utils.models import create_LSTM_model
from src.utils.utils import unison_shuffled_copies


def train_lstm(model, x_train, y_train, x_test, y_test, batch_size, name):
    early_stopping = EarlyStopping(monitor='val_loss', patience=10, mode='min')
    model_path = f"{os.environ['MODEL_DIR']}/lstm/{name}.hdf5"
    mcp_save = ModelCheckpoint(model_path)
    reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=7, verbose=0, min_delta=1e-4,
                                       mode='min')
    model.fit(x_train, y_train, batch_size=batch_size,
              validation_data=(x_test, y_test),
              callbacks=[early_stopping, mcp_save, reduce_lr_loss],
              epochs=100,
              verbose=1)

    saved_model = load_model(model_path)
    return saved_model


def create_lstm(x, y, features, nb_layers, nb_units, name):
    nb_samples = len(x)
    x, y = unison_shuffled_copies(x, y)

    x_train = np.array(x[:int(0.8 * nb_samples)])
    y_train = np.array(y[:int(0.8 * nb_samples)])

    x_test = np.array(x[int(0.8 * nb_samples):])
    y_test = np.array(y[int(0.8 * nb_samples):])

    model = create_LSTM_model(nb_layers, nb_units, len(features), mask_value=float(os.environ["MASK_VALUE"]), timesteps=100)

    train_lstm(model, x_train, y_train, x_test, y_test, int(os.environ['LSTM_BATCH_SIZE']), name)