import os

import numpy as np
import tensorflow as tf
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.models import load_model

from src.utils.utils import unison_shuffled_copies


def get_angles(pos, i, d_model):
    angle_rates = 1 / np.power(10000, (2 * (i // 2)) / np.float32(d_model))
    return pos * angle_rates


def positional_encoding(position, d_model):
    angle_rads = get_angles(np.arange(position)[:, np.newaxis], np.arange(d_model)[np.newaxis, :], d_model)

    # apply sin to even indices in the array; 2i
    angle_rads[:, 0::2] = np.sin(angle_rads[:, 0::2])

    # apply cos to odd indices in the array; 2i+1
    angle_rads[:, 1::2] = np.cos(angle_rads[:, 1::2])
    pos_encoding = angle_rads[np.newaxis, ...]
    return tf.cast(pos_encoding, dtype=tf.float32)


class Encoder(tf.keras.layers.Layer):
    def __init__(self, num_layers=4, d_model=512, num_heads=8, dff=2048,
                 maximum_position_encoding=10000, dropout=0.0, **kwargs):
        super(Encoder, self).__init__()
        self.num_layers = num_layers
        self.d_model = d_model
        self.num_heads = num_heads
        self.dff = dff
        self.maximum_position_encoding = maximum_position_encoding
        self.dropout = dropout

        self.pos = positional_encoding(maximum_position_encoding, d_model)

        self.encoder_layers = [EncoderLayer(d_model=d_model, num_heads=num_heads, dff=dff, dropout=dropout) for _ in
                               range(num_layers)]

        self.dropout_layer = tf.keras.layers.Dropout(dropout)
        self.conv1d = tf.keras.layers.Conv1D(self.d_model, 1, activation='relu')

    def call(self, inputs, training=None):
        x = inputs
        x = self.conv1d(x)
        # positional encoding
        x *= tf.math.sqrt(tf.cast(self.d_model, tf.float32))
        x += self.pos[:, :tf.shape(x)[1], :]

        x = self.dropout_layer(x, training=training)
        # Encoder layer
        for encoder_layer in self.encoder_layers:
            x = encoder_layer(x)

        return x

    def get_config(self):
        config = super().get_config()
        config.update({
            "num_layers": self.num_layers,
            "d_model": self.d_model,
            "num_heads": self.num_heads,
            "dff": self.dff,
            "maximum_position_encoding": self.maximum_position_encoding,
            "dropout": self.dropout,
        })
        return config


class EncoderLayer(tf.keras.layers.Layer):
    def __init__(self, d_model=512, num_heads=8, dff=2048, dropout=0.0, **kwargs):
        super(EncoderLayer, self).__init__()

        self.multi_head_attention = MultiHeadAttention(d_model, num_heads)
        self.dropout_attention = tf.keras.layers.Dropout(dropout)
        self.add_attention = tf.keras.layers.Add()
        self.layer_norm_attention = tf.keras.layers.LayerNormalization(epsilon=1e-6)

        self.dense1 = tf.keras.layers.Dense(dff, activation='relu')
        self.dense2 = tf.keras.layers.Dense(d_model)
        self.dropout_dense = tf.keras.layers.Dropout(dropout)
        self.add_dense = tf.keras.layers.Add()
        self.layer_norm_dense = tf.keras.layers.LayerNormalization(epsilon=1e-6)

    def call(self, inputs, mask=None, training=None):
        attention = self.multi_head_attention([inputs, inputs, inputs], mask=[mask, mask])
        attention = self.dropout_attention(attention, training=training)
        x = self.add_attention([inputs, attention])
        x = self.layer_norm_attention(x)

        ## Feed Forward
        dense = self.dense1(x)
        dense = self.dense2(dense)
        dense = self.dropout_dense(dense, training=training)
        x = self.add_dense([x, dense])
        x = self.layer_norm_dense(x)

        return x


class MultiHeadAttention(tf.keras.layers.Layer):
    def __init__(self, d_model=512, num_heads=8, causal=False, dropout=0.0):
        super(MultiHeadAttention, self).__init__()
        self.causal = causal

        assert d_model % num_heads == 0
        depth = d_model // num_heads

        self.w_query = tf.keras.layers.Dense(d_model)
        self.split_reshape_query = tf.keras.layers.Reshape((-1, num_heads, depth))
        self.split_permute_query = tf.keras.layers.Permute((2, 1, 3))

        self.w_value = tf.keras.layers.Dense(d_model)
        self.split_reshape_value = tf.keras.layers.Reshape((-1, num_heads, depth))
        self.split_permute_value = tf.keras.layers.Permute((2, 1, 3))

        self.w_key = tf.keras.layers.Dense(d_model)
        self.split_reshape_key = tf.keras.layers.Reshape((-1, num_heads, depth))
        self.split_permute_key = tf.keras.layers.Permute((2, 1, 3))

        self.attention = tf.keras.layers.Attention(dropout=dropout)
        self.join_permute_attention = tf.keras.layers.Permute((2, 1, 3))
        self.join_reshape_attention = tf.keras.layers.Reshape((-1, d_model))

        self.dense = tf.keras.layers.Dense(d_model)

    def call(self, inputs, mask=None, training=None):
        q = inputs[0]
        v = inputs[1]
        k = inputs[2] if len(inputs) > 2 else v

        query = self.w_query(q)
        query = self.split_reshape_query(query)
        query = self.split_permute_query(query)

        value = self.w_value(v)
        value = self.split_reshape_value(value)
        value = self.split_permute_value(value)

        key = self.w_key(k)
        key = self.split_reshape_key(key)
        key = self.split_permute_key(key)

        if mask is not None:
            if mask[0] is not None:
                mask[0] = tf.keras.layers.Reshape((-1, 1))(mask[0])
                mask[0] = tf.keras.layers.Permute((2, 1))(mask[0])
            if mask[1] is not None:
                mask[1] = tf.keras.layers.Reshape((-1, 1))(mask[1])
                mask[1] = tf.keras.layers.Permute((2, 1))(mask[1])

        attention = self.attention([query, value, key], mask=mask, use_causal_mask=self.causal)
        attention = self.join_permute_attention(attention)
        attention = self.join_reshape_attention(attention)

        x = self.dense(attention)

        return x


def get_tn(num_layers=4, d_model=128, dff=512, num_heads=8, mask_value=0, input_size=1):
    dropout_rate = 0.1
    nb_inputs = input_size
    nb_outputs = int(os.environ['OUTPUT_SIZE'])
    timesteps = int(os.environ['TIME_SERIES_LENGTH'])

    encoder = Encoder(num_layers=num_layers, d_model=d_model, num_heads=num_heads, dff=dff,
                      dropout=dropout_rate)

    input = tf.keras.layers.Input(shape=(timesteps, nb_inputs))

    mask = tf.keras.layers.Masking(mask_value=mask_value, input_shape=(timesteps, nb_inputs))(input)
    e = encoder(mask)
    prediction_layer = tf.keras.layers.Conv1D(nb_outputs, 1, activation='softmax')(e)

    model = tf.keras.models.Model(inputs=[input], outputs=prediction_layer)

    optimizer = "Adam"
    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'])

    return model

def train_tn(model, x_train, y_train, x_test, y_test, batch_size, name):
    early_stopping = EarlyStopping(monitor='val_loss', patience=10, mode='min')
    model_path = f"{os.environ['MODEL_DIR']}/tn/{name}.hdf5"
    mcp_save = ModelCheckpoint(model_path)
    reduce_lr_loss = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=7, verbose=0, min_delta=1e-4,
                                       mode='min')
    model.fit(x_train, y_train, batch_size=batch_size,
              validation_data=(x_test, y_test),
              callbacks=[early_stopping, mcp_save, reduce_lr_loss],
              epochs=100,
              verbose=1)

    saved_model = load_model(model_path)
    return saved_model


def create_tn(x, y, features, number_of_layers, d_model, dff, number_of_heads, name):
    nb_samples = len(x)
    x, y = unison_shuffled_copies(x, y)

    x_train = np.array(x[:int(0.8 * nb_samples)])
    y_train = np.array(y[:int(0.8 * nb_samples)])

    x_test = np.array(x[int(0.8 * nb_samples):])
    y_test = np.array(y[int(0.8 * nb_samples):])

    model = get_tn(num_layers=number_of_layers, d_model=d_model, dff=dff, num_heads=number_of_heads, mask_value=int(os.environ['MASK_VALUE']),
                   input_size=len(features))

    train_tn(model, x_train, y_train, x_test, y_test, int(os.environ['TN_BATCH_SIZE']), name)