import os
import pickle

from skais.learn.hmm_gmm_classifier import GMMHMMClassifier


def train_HMM(x, y, n_states, name):
    model = GMMHMMClassifier(n_states, max_iter=100, verbose=False)

    model.fit(x, y)

    pickle.dump(model, open(f"{os.environ['MODEL_DIR']}/hmm/{name}.pkl", 'wb'))

