import os
import sys
from pathlib import Path

from dotenv import load_dotenv
from skais.ais.ais_points import AISPoints

from src.models.lstm import create_lstm
from src.utils.data_import import load_dataset_from_csv
from src.utils.experiment_tools import normalize_set_of_trajectories, convert_to_vectors, split_to_size_list, \
    compute_features_on_trajectories_interpolated
from src.utils.features_list import FEATURES_LIST

if __name__ == '__main__':
    run = int(sys.argv[1])
    load_dotenv()
    Path(f"{os.environ['MODEL_DIR']}/lstm/").mkdir(parents=True, exist_ok=True)

    number_of_layers = int(os.environ["LSTM_CUSTOM_NUMBER_OF_LAYERS"])
    number_of_units = int(os.environ["LSTM_CUSTOM_NUMBER_OF_UNITS"])

    trajectories_train = load_dataset_from_csv(os.environ["NE_DATA"], [int(i) for i in os.environ["NE_TRAIN_INDICES"].split(" ")])

    compute_features_on_trajectories_interpolated(trajectories_train, int(os.environ["LSTM_CUSTOM_TIME_WINDOW"]), tqdm_position=1)

    points = AISPoints.fuse(trajectories_train)
    normalisation_factors = points.normalize(
        standardization_features=['delta_sog', 'std_cog', 'std_hdg', 'std_delta_cog',
                                  'std_delta_heading',
                                  'std_pos', 'std_sog'],
        third_quartile_features=['sog'],
        divide_by_value=[('drift', 180), ('delta_cog', 180), ('delta_heading', 180),
                         ('latitude', 90), ('longitude', 180)]
    )
    normalize_set_of_trajectories(trajectories_train, normalisation_factors)

    x_train, y_train = convert_to_vectors(trajectories_train, FEATURES_LIST)
    x_train, y_train = split_to_size_list(x_train, y_train, size=int(os.environ["TIME_SERIES_LENGTH"]), value=float(os.environ["MASK_VALUE"]))

    name = f"CUSTOM_{run}"

    create_lstm(x_train, y_train, FEATURES_LIST, number_of_layers, number_of_units, name)