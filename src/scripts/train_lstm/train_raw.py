import os
import sys
from pathlib import Path

from dotenv import load_dotenv
from skais.ais.ais_points import AISPoints

from src.models.lstm import create_lstm
from src.utils.data_import import load_dataset_from_csv
from src.utils.experiment_tools import normalize_set_of_trajectories, convert_to_vectors, split_to_size_list
from src.utils.features_list import FEATURES_COMBINATIONS

if __name__ == '__main__':
    config = int(sys.argv[1])
    run = int(sys.argv[2])
    load_dotenv()
    Path(f"{os.environ['MODEL_DIR']}/lstm/").mkdir(parents=True, exist_ok=True)

    if config == 0:
        number_of_layers = int(os.environ["LSTM_SOG_NUMBER_OF_LAYERS"])
        number_of_units = int(os.environ["LSTM_SOG_NUMBER_OF_UNITS"])
    elif config == 1:
        number_of_layers = int(os.environ["LSTM_COG_NUMBER_OF_LAYERS"])
        number_of_units = int(os.environ["LSTM_COG_NUMBER_OF_UNITS"])
    elif config == 2:
        number_of_layers = int(os.environ["LSTM_HEADING_NUMBER_OF_LAYERS"])
        number_of_units = int(os.environ["LSTM_HEADING_NUMBER_OF_UNITS"])
    elif config == 3:
        number_of_layers = int(os.environ["LSTM_POSITION_NUMBER_OF_LAYERS"])
        number_of_units = int(os.environ["LSTM_POSITION_NUMBER_OF_UNITS"])
    elif config == 4:
        number_of_layers = int(os.environ["LSTM_RAW_NO_SOG_NUMBER_OF_LAYERS"])
        number_of_units = int(os.environ["LSTM_RAW_NO_SOG_NUMBER_OF_UNITS"])
    elif config == 5:
        number_of_layers = int(os.environ["LSTM_RAW_NUMBER_OF_LAYERS"])
        number_of_units = int(os.environ["LSTM_RAW_NUMBER_OF_UNITS"])
    else:
        print("Config not recognized")
        exit(1)

    trajectories_train = load_dataset_from_csv(os.environ["NE_DATA"], [int(i) for i in os.environ["NE_TRAIN_INDICES"].split(" ")])

    points = AISPoints.fuse(trajectories_train)
    normalisation_factors = points.normalize(
        third_quartile_features=['sog'],
        divide_by_value=[('latitude', 90), ('longitude', 180), ('heading', 180), ('cog', 180)]
    )

    normalize_set_of_trajectories(trajectories_train, normalisation_factors)

    x_train, y_train = convert_to_vectors(trajectories_train, FEATURES_COMBINATIONS[config])
    x_train, y_train = split_to_size_list(x_train, y_train, size=int(os.environ["TIME_SERIES_LENGTH"]), value=float(os.environ["MASK_VALUE"]))

    features = FEATURES_COMBINATIONS[config]
    name = f"{config}_{run}"

    create_lstm(x_train, y_train, features, number_of_layers, number_of_units, run)