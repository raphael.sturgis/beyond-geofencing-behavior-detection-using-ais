import os
import pickle

import numpy as np
import pandas as pd
from dotenv import load_dotenv
from skais.ais.ais_points import AISPoints
from skais.learn.hmm_gmm_classifier import split_trajectories
from tqdm import tqdm

from src.scripts.extract_predictions import prediction_pipelines

behaviours = ['Underway', 'Moored', 'At Anchor', 'Drifting']
factor = 0.5
disable = True
model_ids = [0, 2, 6, 4, 9, -1]

if __name__ == '__main__':
    load_dotenv()
    data_dir = os.environ["DATA_DIR"]
    trajectories = pickle.load(open(f"{data_dir}/predictions/lax_predictions.pkl", 'rb'))

    data = {
        'name': []
    }

    for b in behaviours:
        data[f'{b} events recognized'] = []
        data[f'{b} events missed'] = []
        data[f'{b} events recognized std'] = []
        data[f'{b} events missed std'] = []

    points = AISPoints.fuse(trajectories).df
    y_true = points['label'].to_numpy()

    print("| Model | Underway | Moored | At Anchor | Drifting |\n"
          "|---------------------:|---------------------:|-------------------:|----------------------:|---------------------:|")

    iterator = tqdm([prediction_pipelines[i] for i in model_ids], colour="blue", position=0, leave=True, disable=disable)
    for name, name_pretty, _, _, runs, _ in iterator:
        iterator.set_description(name)

        data['name'].append(name_pretty)

        missed_stack = []
        recognized_stack = []

        for i in tqdm(range(runs), colour='yellow', position=1, leave=False, disable=disable):
            y_pred = points[f'prediction_{name}_{i}'].to_numpy()

            missed = [0 for _ in behaviours]
            recognized = [0 for _ in behaviours]

            split, _ = split_trajectories(y_pred, y_true, 4)
            for i, _ in enumerate(tqdm(behaviours, colour='green', position=2, leave=False, disable=disable)):
                for event in tqdm(split[i], colour='magenta', position=3, leave=False, disable=disable):
                    unique, counts = np.unique(event, return_counts=True)
                    d = dict(zip(unique, counts))
                    if i not in d:
                        missed[i] += 1
                    else:
                        if (d[i]/len(event)) > factor:
                            recognized[i] += 1
                        else:
                            missed[i] += 1
            missed_stack.append(missed)
            recognized_stack.append(recognized)

        missed_mean = np.mean(missed_stack, axis=0)
        missed_std = np.std(missed_stack, axis=0)
        recognized_mean = np.mean(recognized_stack, axis=0)
        recognized_std = np.std(recognized_stack, axis=0)

        for i, b in enumerate(behaviours):
            data[f'{b} events recognized'].append(recognized_mean[i])
            data[f'{b} events missed'].append(missed_mean[i])
            data[f'{b} events recognized std'].append(recognized_std[i])
            data[f'{b} events missed std'].append(missed_std[i])

        print(
            f"{name_pretty} | {recognized_mean[0]/(recognized_mean[0]+missed_mean[0]):.3f} ({recognized_std[0]/(recognized_mean[0]+missed_mean[0]):.3f}) | "
            f"{recognized_mean[1]/(recognized_mean[1]+missed_mean[1]):.3f} ({recognized_std[1]/(recognized_mean[1]+missed_mean[1]):.3f}) | "
            f"{recognized_mean[2]/(recognized_mean[2]+missed_mean[2]):.3f} ({recognized_std[2]/(recognized_mean[2]+missed_mean[2]):.3f}) | "
            f"{recognized_mean[3]/(recognized_mean[3]+missed_mean[3]):.3f} ({recognized_std[3]/(recognized_mean[3]+missed_mean[3]):.3f}) | "
        )
    df = pd.DataFrame(data)
    df.fillna(0, inplace=True)
    print(df.to_markdown(index=False))
