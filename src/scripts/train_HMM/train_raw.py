import os
import sys
from pathlib import Path

from dotenv import load_dotenv
from skais.ais.ais_points import AISPoints

from src.models.hmm import train_HMM
from src.utils.data_import import load_dataset_from_csv
from src.utils.experiment_tools import normalize_set_of_trajectories, convert_to_vectors_not_one_hot
from src.utils.features_list import FEATURES_COMBINATIONS

if __name__ == '__main__':
    config = int(sys.argv[1])
    run = int(sys.argv[2])
    load_dotenv()
    Path(f"{os.environ['MODEL_DIR']}/hmm/").mkdir(parents=True, exist_ok=True)

    if config == 0:
        n_states = [int(i) for i in os.environ["HMM_SOG_NUMBER_OF_STATES"].split(" ")]
    elif config == 1:
        n_states = [int(i) for i in os.environ["HMM_COG_NUMBER_OF_STATES"].split(" ")]
    elif config == 2:
        n_states = [int(i) for i in os.environ["HMM_HEADING_NUMBER_OF_STATES"].split(" ")]
    elif config == 3:
        n_states = [int(i) for i in os.environ["HMM_POSITION_NUMBER_OF_STATES"].split(" ")]
    elif config == 4:
        n_states = [int(i) for i in os.environ["HMM_RAW_NO_SOG_NUMBER_OF_STATES"].split(" ")]
    elif config == 5:
        n_states = [int(i) for i in os.environ["HMM_RAW_NUMBER_OF_STATES"].split(" ")]
    else:
        print("Config not recognized")
        exit(1)

    trajectories_train = load_dataset_from_csv(os.environ["NE_DATA"], [int(i) for i in os.environ["NE_TRAIN_INDICES"].split(" ")])

    points = AISPoints.fuse(trajectories_train)
    normalisation_factors = points.normalize(
        third_quartile_features=['sog'],
        divide_by_value=[('latitude', 90), ('longitude', 180), ('heading', 180), ('cog', 180)]
    )

    normalize_set_of_trajectories(trajectories_train, normalisation_factors)

    x_train, y_train = convert_to_vectors_not_one_hot(trajectories_train, FEATURES_COMBINATIONS[config])

    name = f"{config}_{run}"

    train_HMM(x_train, y_train, n_states, name)