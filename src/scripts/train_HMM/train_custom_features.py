import os
import sys
from pathlib import Path

from dotenv import load_dotenv
from skais.ais.ais_points import AISPoints

from src.models.hmm import train_HMM
from src.utils.data_import import load_dataset_from_csv
from src.utils.experiment_tools import normalize_set_of_trajectories, compute_features_on_trajectories_interpolated, \
    convert_to_vectors_not_one_hot
from src.utils.features_list import FEATURES_LIST

if __name__ == '__main__':
    run = int(sys.argv[1])
    load_dotenv()
    Path(f"{os.environ['MODEL_DIR']}/hmm/").mkdir(parents=True, exist_ok=True)

    n_states = [int(i) for i in os.environ["HMM_CUSTOM_NUMBER_OF_STATES"].split(" ")]

    trajectories_train = load_dataset_from_csv(os.environ["NE_DATA"], [int(i) for i in os.environ["NE_TRAIN_INDICES"].split(" ")])

    compute_features_on_trajectories_interpolated(trajectories_train, int(os.environ["HMM_CUSTOM_TIME_WINDOW"]), tqdm_position=1)

    points = AISPoints.fuse(trajectories_train)
    normalisation_factors = points.normalize(
        standardization_features=['delta_sog', 'std_cog', 'std_hdg', 'std_delta_cog',
                                  'std_delta_heading',
                                  'std_pos', 'std_sog'],
        third_quartile_features=['sog'],
        divide_by_value=[('drift', 180), ('delta_cog', 180), ('delta_heading', 180),
                         ('latitude', 90), ('longitude', 180)]
    )
    normalize_set_of_trajectories(trajectories_train, normalisation_factors)

    x_train, y_train = convert_to_vectors_not_one_hot(trajectories_train, FEATURES_LIST)

    name = f"CUSTOM_{run}"

    train_HMM(x_train, y_train, n_states, name)