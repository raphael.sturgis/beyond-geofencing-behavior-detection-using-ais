import os
import pickle
from pathlib import Path

import numpy as np
from dotenv import load_dotenv
from sklearn.metrics import accuracy_score, f1_score
from tqdm import tqdm

from src.scripts.extract_predictions import prediction_pipelines


def get_acc_and_f1(trajectories, column):
    y_true = []
    y_pred = []
    for t in trajectories:
        y_true.append(t.df.label.to_numpy())
        y_pred.append(t.df[column].to_numpy())
    y_true = np.concatenate(y_true)
    y_pred = np.concatenate(y_pred)

    return accuracy_score(y_true, y_pred), f1_score(y_true, y_pred, average='macro')

if __name__ == '__main__':
    load_dotenv()
    data_dir = os.environ["DATA_DIR"]

    trajectories_LAX = pickle.load(open(f"{data_dir}/predictions/lax_predictions.pkl", 'rb'))
    trajectories_NE = pickle.load(open(f"{data_dir}/predictions/ne_predictions.pkl", 'rb'))

    iterator = tqdm(prediction_pipelines, colour="magenta")
    data = []

    results = []
    for name, name_pretty, _, _, runs, _ in iterator:
        iterator.set_description(name)

        accs_lax = []
        f1s_lax = []
        accs_ne = []
        f1s_ne = []
        for i in tqdm(range(runs), colour="blue", leave=False):
            column_name = f'prediction_{name}_{i}'
            acc_lax, f1_lax = get_acc_and_f1(trajectories_LAX, column_name)
            accs_lax.append(acc_lax)
            f1s_lax.append(f1_lax)

            acc_ne, f1_ne = get_acc_and_f1(trajectories_NE, column_name)
            accs_ne.append(acc_ne)
            f1s_ne.append(f1_ne)

        acc_lax = np.mean(accs_lax)
        f1_lax = np.mean(f1s_lax)
        acc_ne = np.mean(accs_ne)
        f1_ne = np.mean(f1s_ne)

        acc_lax_std = np.std(accs_lax)
        f1_lax_std = np.std(f1s_lax)
        acc_ne_std = np.std(accs_ne)
        f1_ne_std = np.std(f1s_ne)

        results.append((acc_lax, acc_lax_std, f1_lax, f1_lax_std, acc_ne, acc_ne_std, f1_ne, f1_ne_std))

    print("| Model | Accuracy LAX | Macro F1 LAX | Accuracy NE | Macro F1 NE\n"
          "|---------------------:|---------------------:|-------------------:|----------------------:|---------------------:|")

    for result, pipeline in zip(results, prediction_pipelines):
        acc_lax, acc_lax_std, f1_lax, f1_lax_std, acc_ne, acc_ne_std, f1_ne, f1_ne_std = result
        name, name_pretty, _, _, _, _, = pipeline
        print(f"{name_pretty} | {acc_lax:.3f} ({acc_lax_std:.3f}) | {f1_lax:.3f} ({f1_lax_std:.3f}) | "
              f"{acc_ne:.3f} ({acc_ne_std:.3f}) | {f1_ne:.3f} ({f1_ne_std:.3f})")




