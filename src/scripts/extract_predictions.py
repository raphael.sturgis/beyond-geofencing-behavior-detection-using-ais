from copy import deepcopy
from pathlib import Path
import os

from src.utils.data_import import load_dataset_from_csv

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import warnings
from tqdm import tqdm
from dotenv import load_dotenv

from src.utils.experiment_tools import match_prediction_to_trajectories
from predictors import *

warnings.filterwarnings("ignore")
time_window = 30
prediction_pipelines = [
    ("navstatus", "Navigation Status", navstatus_preprocessing, navstatus_trajectory_predictor, 1, None),

    ("dt_raw", "Decision Tree with Raw Features", dt_raw_preprocessing, dt_predictor, 10, None),
    ("rf_raw", "Random Forest with Raw Features", rf_raw_preprocessing, rf_predictor, 10, None),
    ("lstm_raw", "LSTM with Raw Features", sequence_raw_preprocessing, lstm_raw_trajectory_predictor, 10, None),
    ("tn_raw", "Transformer Network with Raw Features", sequence_raw_preprocessing, tn_raw_trajectory_predictor, 10,
     None),

    ("dt_custom", "Decision Tree with Custom Features", dt_custom_preprocessing, dt_predictor, 10, time_window),
    ("rf_custom", "Random Forest with Custom Features", rf_custom_preprocessing, rf_predictor, 10, time_window),
    ("hmm_custom", "HMM with Custom Features", hmm_custom_preprocessing, hmm_custom_predictor, 10, time_window),
    ("lstm_custom", "LSTM with Custom Features", sequence_custom_preprocessing, lstm_custom_trajectory_predictor, 10,
     int(os.environ['LSTM_CUSTOM_TIME_WINDOW'])),
    ("tn_custom", "Transformer Network with Custom Features", sequence_custom_preprocessing,
     tn_custom_trajectory_predictor, 10, int(os.environ['TN_CUSTOM_TIME_WINDOW'])),

    ("lstm_augmented", "LSTM with Augmented Data", sequence_raw_preprocessing, lstm_raw_trajectory_predictor_augmented,
     10, None),
    ("tn_augmented", "Transformer Network with Augmented Data", sequence_raw_preprocessing,
     tn_raw_trajectory_predictor_augmented, 10, None),
]

keep_previous_prediction = True

if __name__ == '__main__':
    load_dotenv()
    data_dir = os.environ["DATA_DIR"]
    Path(data_dir).mkdir(parents=True, exist_ok=True)
    Path(f"{data_dir}/predictions").mkdir(parents=True, exist_ok=True)

    trajectories_train = load_dataset_from_csv(os.environ["NE_DATA"], os.environ["NE_TRAIN_INDICES"].split(" "))
    trajectories_NE = load_dataset_from_csv(os.environ["NE_DATA"], os.environ["NE_TEST_INDICES"].split(" "))
    trajectories_LAX = load_dataset_from_csv(os.environ["LAX_DATA"])

    for name, name_pretty, preprocessing, predictor, n_runs, aux_data in tqdm(prediction_pipelines, colour="green", position=0, leave=True):
        calculated = True
        for i in range(n_runs):
            if f'prediction_{name}_{i}' not in trajectories_LAX[-1].df.columns:
                calculated = False
                break
            if f'prediction_{name}_{i}' not in trajectories_NE[-1].df.columns:
                calculated = False
                break

        if calculated:
            continue

        prediction_data_LAX = preprocessing(deepcopy(trajectories_train), deepcopy(trajectories_LAX), 'LAX', aux_data)
        prediction_data_NE = preprocessing(deepcopy(trajectories_train), deepcopy(trajectories_NE), 'NE', aux_data)

        for i in tqdm(range(n_runs), colour="blue", position=1, leave=False):
            y_predict_LAX = predictor(prediction_data_LAX, run=i)
            match_prediction_to_trajectories(trajectories_LAX, y_predict_LAX, name=f'prediction_{name}_{i}',
                                             tqdm_disable=False, tqdm_position=2)

            y_predict_NE = predictor(prediction_data_NE, run=i)
            match_prediction_to_trajectories(trajectories_NE, y_predict_NE, name=f'prediction_{name}_{i}',
                                             tqdm_disable=False, tqdm_position=2)

        pickle.dump(trajectories_LAX, open(f"{data_dir}/predictions/lax_predictions.pkl", 'wb'))
        pickle.dump(trajectories_NE, open(f"{data_dir}/predictions/ne_predictions.pkl", 'wb'))
