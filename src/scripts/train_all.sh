#!/bin/bash

export PYTHONPATH="${PYTHONPATH}:../../"

for i in {0..9}
do
  for j in {0..5}
  do
    python3 train_HMM/train_raw.py "$j" "$i"
    python3 train_lstm/train_raw.py "$j" "$i"
    python3 train_transformer/train_raw.py "$j" "$i"
  done

  python3 train_HMM/train_custom_features.py "$i"
  
  python3 train_lstm/train_augmented.py "$i"
  python3 train_lstm/train_custom_features.py "$i"
  
  python3 train_transformer/train_augmented.py "$i"
  python3 train_transformer/train_custom_features.py "$i"
done

