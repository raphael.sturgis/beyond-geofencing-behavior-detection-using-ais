import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from sklearn.tree import DecisionTreeClassifier
from tqdm import tqdm

from src.utils.experiment_tools import convert_to_vectors_not_one_hot
from src.utils.features_list import FEATURES_COMBINATIONS
from src.utils.utils import unison_shuffled_copies, evaluate_model


def generate_data(features, trajectories_train, trajectories_test_LAX, trajectories_test_NE):
    x_train, y_train = convert_to_vectors_not_one_hot(trajectories_train, features)
    x_train = np.concatenate(x_train)
    y_train = np.concatenate(y_train)

    x_test_lax, y_test_lax = convert_to_vectors_not_one_hot(trajectories_test_LAX, features)
    x_test_lax = np.concatenate(x_test_lax)
    y_test_lax = np.concatenate(y_test_lax)

    x_test_ne, y_test_ne = convert_to_vectors_not_one_hot(trajectories_test_NE, features)
    x_test_ne = np.concatenate(x_test_ne)
    y_test_ne = np.concatenate(y_test_ne)

    return x_train, y_train, x_test_lax, y_test_lax, x_test_ne, y_test_ne

def train_model(params, x_train, y_train):
    dt = DecisionTreeClassifier(max_depth=params['max_depth'], criterion=params['criterion'], class_weight='balanced',
                                splitter="best")
    dt.fit(x_train, y_train)
    return dt
def draw_accuracy_graph(features, average_accs_NE, std_accs_NE, average_accs_LAX, std_accs_LAX):
    barWidth = 0.3
    X = np.arange(len(features))

    plt.bar(X - barWidth / 2, average_accs_NE, width=barWidth, yerr=std_accs_NE, label='NE')
    plt.bar(X + barWidth / 2, average_accs_LAX, width=barWidth, yerr=std_accs_LAX, label='LAX')

    plt.xticks(X, [i for i in range(1, 7)])
    plt.xlabel('Configuration')
    plt.ylabel('accuracy')
    plt.legend()

    plt.show()


def draw_dt_accuracy_per_features(trajectories_train, trajectories_test_LAX, trajectories_test_NE):
    tree_para = {'criterion': ['gini', 'entropy'],
                 'max_depth': [4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 20, 30, 40, 50, 70, 90, 120, 150]}
    for features in FEATURES_COMBINATIONS:
        x_train, y_train, x_test_lax, y_test_lax, x_test_ne, y_test_ne = generate_data(
            features, trajectories_train, trajectories_test_LAX, trajectories_test_NE
        )
        accs_LAX = []
        accs_NE = []
        clf = GridSearchCV(DecisionTreeClassifier(class_weight='balanced', splitter="best"), tree_para, cv=5, verbose=2,
                           n_jobs=-1)
        clf.fit(x_train, y_train)
        performances = []
        for _ in tqdm(range(10)):
            try:
                x_train, y_train = unison_shuffled_copies(x_train, y_train)
                model = train_model(clf.best_params_, x_train[:int(0.8 * len(x_train))],
                                    y_train[:int(0.8 * len(x_train))])
                acc, f1, c_mat = evaluate_model(model, x_test_lax, y_test_lax)
                accs_LAX.append(acc)
                acc, _, _ = evaluate_model(model, x_test_ne, y_test_ne)
                accs_NE.append(acc)
            except Exception as e:
                print(f"Failed : {e}")
            performances.append(
                {
                    'features': features,
                    'average_acc_lax': np.mean(accs_LAX),
                    'std_acc_lax': np.std(accs_LAX),
                    'average_acc_ne': np.mean(accs_NE),
                    'std_acc_ne': np.std(accs_NE),
                }
            )
    features = [perf["features"] for perf in performances]
    average_accs_LAX = [perf["average_acc_lax"] for perf in performances]
    std_accs_LAX = [perf["std_acc_lax"] for perf in performances]
    average_accs_NE = [perf["average_acc_ne"] for perf in performances]
    std_accs_NE = [perf["std_acc_ne"] for perf in performances]
    draw_accuracy_graph(features, average_accs_NE, std_accs_NE, average_accs_LAX, std_accs_LAX)


if __name__ == '__main__':
    draw_dt_accuracy_per_features()