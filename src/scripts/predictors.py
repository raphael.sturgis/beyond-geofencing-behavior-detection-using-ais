import pickle
from copy import copy
import os

from skais.learn.hmm_gmm_classifier import GMMHMMClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.tree import DecisionTreeClassifier

import numpy as np
from skais.ais.ais_points import AISPoints
from skais.utils.config import get_data_path
from keras.models import load_model

from src.models.transformer import Encoder, EncoderLayer, MultiHeadAttention
from src.utils.features import compute_selected_features_on_trajectories

from src.utils.experiment_tools import normalize_set_of_trajectories, convert_to_vectors, split_to_size_list, \
    convert_to_vectors_not_one_hot
from src.utils.utils import unison_shuffled_copies

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

def navstatus_preprocessing(_, trajectories, __, ___):
    return trajectories


def dt_raw_preprocessing(trajectories_train, trajectories_test, dataset, _):
    features = ['sog', 'cog', 'heading', 'longitude', 'latitude']
    fname = f"raw_convert_to_vectors_not_one_hot_{dataset}.pkl"
    if os.path.isfile(fname):
        x_train, y_train, x_test, y_test = pickle.load(open(fname, 'rb'))
    else:
        x_train, y_train = convert_to_vectors_not_one_hot(trajectories_train, features, tqdm_disable=True)
        x_train = np.concatenate(x_train)
        y_train = np.concatenate(y_train)

        x_test, y_test = convert_to_vectors_not_one_hot(trajectories_test, features, tqdm_disable=True)
        x_test = np.concatenate(x_test)
        y_test = np.concatenate(y_test)
        pickle.dump((x_train, y_train, x_test, y_test), open(fname, 'wb'))

    tree_para = {'criterion': ['gini', 'entropy'],
                 'max_depth': [1, 2, 5, 10, 20, 50]}
    clf = GridSearchCV(DecisionTreeClassifier(class_weight='balanced', splitter="best"), tree_para, cv=5, verbose=0,
                       n_jobs=-1)
    clf.fit(x_train, y_train)

    return x_train, y_train, x_test, y_test, clf.best_params_


def dt_custom_preprocessing(trajectories_train, trajectories_test, dataset, time_window):
    features = ['sog', 'drift', 'delta_sog', 'delta_cog', 'delta_heading', 'std_cog',
                'std_hdg', 'std_delta_cog', 'std_delta_heading', 'std_pos', 'std_sog']

    fname = f"custom_convert_to_vectors_not_one_hot_{dataset}_{time_window}.pkl"
    if os.path.isfile(fname):
        x_train, y_train, x_test, y_test = pickle.load(open(fname, 'rb'))
    else:
        trajectories_train = compute_selected_features_on_trajectories(trajectories_train, time_window, min_size=-1,
                                                                       tqdm_disable=True)
        trajectories_test = compute_selected_features_on_trajectories(trajectories_test, time_window, min_size=-1,
                                                                      tqdm_disable=True)
    
        points = AISPoints.fuse(trajectories_train)
        normalisation_factors = points.normalize(
            standardization_features=['delta_sog', 'std_cog', 'std_hdg', 'std_delta_cog',
                                      'std_delta_heading',
                                      'std_pos', 'std_sog'],
            third_quartile_features=['sog'],
            divide_by_value=[('drift', 180), ('delta_cog', 180), ('delta_heading', 180),
                             ('latitude', 90), ('longitude', 180)]
        )
        normalize_set_of_trajectories(trajectories_train, normalisation_factors)
        normalize_set_of_trajectories(trajectories_test, normalisation_factors)
    
        x_train, y_train = convert_to_vectors_not_one_hot(trajectories_train, features, tqdm_disable=True)
        x_train = np.concatenate(x_train)
        y_train = np.concatenate(y_train)
    
        x_test, y_test = convert_to_vectors_not_one_hot(trajectories_test, features, tqdm_disable=True)
        x_test = np.concatenate(x_test)
        y_test = np.concatenate(y_test)

        pickle.dump((x_train, y_train, x_test, y_test), open(fname, 'wb'))

    tree_para = {'criterion': ['gini', 'entropy'],
                 'max_depth': [1, 2, 5, 10, 20, 50]}
    clf = GridSearchCV(DecisionTreeClassifier(class_weight='balanced', splitter="best"), tree_para, cv=5, verbose=0,
                       n_jobs=-1)
    clf.fit(x_train, y_train)

    return x_train, y_train, x_test, y_test, clf.best_params_


def rf_raw_preprocessing(trajectories_train, trajectories_test, dataset, _):
    features = ['sog', 'cog', 'heading', 'longitude', 'latitude']
    fname = f"raw_convert_to_vectors_not_one_hot_{dataset}.pkl"
    if os.path.isfile(fname):
        x_train, y_train, x_test, y_test = pickle.load(open(fname, 'rb'))
    else:
        x_train, y_train = convert_to_vectors_not_one_hot(trajectories_train, features, tqdm_disable=True)
        x_train = np.concatenate(x_train)
        y_train = np.concatenate(y_train)
    
        x_test, y_test = convert_to_vectors_not_one_hot(trajectories_test, features, tqdm_disable=True)
        x_test = np.concatenate(x_test)
        y_test = np.concatenate(y_test)

        pickle.dump((x_train, y_train, x_test, y_test), open(fname, 'wb'))

    rf_para = {'n_estimators': [5, 10, 20, 50, 100],
               'max_depth': [1, 2, 5, 10, 20, 50]}
    clf = GridSearchCV(RandomForestClassifier(class_weight='balanced'), rf_para, cv=5, verbose=0,
                       n_jobs=-1)
    clf.fit(x_train, y_train)

    return x_train, y_train, x_test, y_test, clf.best_params_


def rf_custom_preprocessing(trajectories_train, trajectories_test, dataset, time_window):
    features = ['sog', 'drift', 'delta_sog', 'delta_cog', 'delta_heading', 'std_cog',
                'std_hdg', 'std_delta_cog', 'std_delta_heading', 'std_pos', 'std_sog']
    fname = f"custom_convert_to_vectors_not_one_hot_{dataset}_{time_window}.pkl"
    if os.path.isfile(fname):
        x_train, y_train, x_test, y_test = pickle.load(open(fname, 'rb'))
    else:
        trajectories_train = compute_selected_features_on_trajectories(trajectories_train, time_window, min_size=-1,
                                                                       tqdm_disable=True)
        trajectories_test = compute_selected_features_on_trajectories(trajectories_test, time_window, min_size=-1,
                                                                      tqdm_disable=True)
    
        points = AISPoints.fuse(trajectories_train)
        normalisation_factors = points.normalize(
            standardization_features=['delta_sog', 'std_cog', 'std_hdg', 'std_delta_cog',
                                      'std_delta_heading',
                                      'std_pos', 'std_sog'],
            third_quartile_features=['sog'],
            divide_by_value=[('drift', 180), ('delta_cog', 180), ('delta_heading', 180),
                             ('latitude', 90), ('longitude', 180)]
        )
        normalize_set_of_trajectories(trajectories_train, normalisation_factors)
        normalize_set_of_trajectories(trajectories_test, normalisation_factors)
    
        x_train, y_train = convert_to_vectors_not_one_hot(trajectories_train, features, tqdm_disable=True)
        x_train = np.concatenate(x_train)
        y_train = np.concatenate(y_train)
    
        x_test, y_test = convert_to_vectors_not_one_hot(trajectories_test, features, tqdm_disable=True)
        x_test = np.concatenate(x_test)
        y_test = np.concatenate(y_test)

        pickle.dump((x_train, y_train, x_test, y_test), open(fname, 'wb'))

    rf_para = {'n_estimators': [5, 10, 20, 50, 100],
               'max_depth': [1, 2, 5, 10, 20, 50]}
    clf = GridSearchCV(RandomForestClassifier(class_weight='balanced'), rf_para, cv=5, verbose=0,
                       n_jobs=-1)
    clf.fit(x_train, y_train)

    return x_train, y_train, x_test, y_test, clf.best_params_


def sequence_raw_preprocessing(trajectories_train, trajectories_test, dataset, _):
    fname = f"sequence_vector_{dataset}.pkl"
    if os.path.isfile(fname):
        x_raw, y_raw = pickle.load(open(fname, 'rb'))
    else:
        points = AISPoints.fuse(trajectories_train)
        normalisation_factors = points.normalize(
            third_quartile_features=['sog'],
            divide_by_value=[('latitude', 90), ('longitude', 180), ('heading', 180), ('cog', 180)]
        )

        normalize_set_of_trajectories(trajectories_test, normalisation_factors)

        features_raw = ['sog', 'cog', 'heading', 'longitude', 'latitude']
        x_raw, y_raw = convert_to_vectors(trajectories_test, features_raw)
        x_raw, y_raw = split_to_size_list(x_raw, y_raw, size=int(os.environ['TIME_SERIES_LENGTH']), value=float(os.environ['MASK_VALUE']))
        pickle.dump((x_raw, y_raw), open(fname, 'wb'))
    return x_raw, y_raw


def hmm_custom_preprocessing(trajectories_train, trajectories_test, dataset, time_window):
    features = ['sog', 'drift', 'delta_sog', 'delta_cog', 'delta_heading', 'std_cog',
                'std_hdg', 'std_delta_cog', 'std_delta_heading', 'std_pos', 'std_sog']
    fname = f"custom_convert_to_vectors_not_one_hot_not_concatenated_{dataset}_{time_window}.pkl"
    if os.path.isfile(fname):
        x_train, y_train, x_test, y_test = pickle.load(open(fname, 'rb'))
    else:
        trajectories_train = compute_selected_features_on_trajectories(trajectories_train, time_window, min_size=-1,
                                                                       tqdm_disable=True)
        trajectories_test = compute_selected_features_on_trajectories(trajectories_test, time_window,
                                                                      min_size=-1, tqdm_disable=True)

        points = AISPoints.fuse(trajectories_train)
        normalisation_factors = points.normalize(
            standardization_features=['delta_sog', 'std_cog', 'std_hdg', 'std_delta_cog',
                                      'std_delta_heading',
                                      'std_pos', 'std_sog'],
            third_quartile_features=['sog'],
            divide_by_value=[('drift', 180), ('delta_cog', 180), ('delta_heading', 180),
                             ('latitude', 90), ('longitude', 180)]
        )

        normalize_set_of_trajectories(trajectories_train, normalisation_factors)
        normalize_set_of_trajectories(trajectories_test, normalisation_factors)

        x_train, y_train = convert_to_vectors_not_one_hot(trajectories_train, features)
        x_test, y_test = convert_to_vectors_not_one_hot(trajectories_test, features)
        pickle.dump((x_train, y_train, x_test, y_test), open(fname, 'wb'))

    return x_train, y_train, x_test, y_test


def sequence_custom_preprocessing(trajectories_train, trajectories_test, dataset, time_window):
    features = ['sog', 'drift', 'delta_sog', 'delta_cog', 'delta_heading', 'std_cog',
                'std_hdg', 'std_delta_cog', 'std_delta_heading', 'std_pos', 'std_sog']
    fname = f"custom_sequence_{dataset}_{time_window}.pkl"
    if os.path.isfile(fname):
        result = pickle.load(open(fname, 'rb'))
    else:
        trajectories_train = compute_selected_features_on_trajectories(trajectories_train, time_window, min_size=-1,
                                                                       tqdm_disable=True)
        trajectories_test = compute_selected_features_on_trajectories(trajectories_test, time_window,
                                                                      min_size=-1, tqdm_disable=True)

        points = AISPoints.fuse(trajectories_train)
        normalisation_factors = points.normalize(
            standardization_features=['delta_sog', 'std_cog', 'std_hdg', 'std_delta_cog',
                                      'std_delta_heading',
                                      'std_pos', 'std_sog'],
            third_quartile_features=['sog'],
            divide_by_value=[('drift', 180), ('delta_cog', 180), ('delta_heading', 180),
                             ('latitude', 90), ('longitude', 180)]
        )
        normalize_set_of_trajectories(trajectories_test, normalisation_factors)

        x, y = convert_to_vectors(trajectories_test, features)
        result = split_to_size_list(x, y, size=int(os.environ['TIME_SERIES_LENGTH']), value=float(os.environ['MASK_VALUE']))
        pickle.dump(result, open(fname, 'wb'))

    return result


def navstatus_trajectory_predictor(trajectories, run=0):
    _ = run
    X = [t.df[['navstatus']].to_numpy().astype(int) for t in trajectories]
    x_true = []
    for x in X:
        new_x = copy(x)
        new_x[x == 0] = 0
        new_x[x == 1] = 2
        new_x[x == 2] = 3
        new_x[x == 3] = 4
        new_x[x == 4] = 4
        new_x[x == 5] = 1
        new_x[x >= 6] = 4
        x_true.append(new_x)

    return np.concatenate(x_true)


def lstm_custom_trajectory_predictor(data, run=0):
    batch_size = int(os.environ['LSTM_BATCH_SIZE_INFERENCE'])

    x, y = data

    name = f"CUSTOM_{run}"
    model = load_model(f"{os.environ['MODEL_DIR']}/lstm/{name}.hdf5")
    predictions = model.predict(np.array(x), batch_size=batch_size, verbose=0)

    M = np.ma.masked_equal(np.concatenate(x)[:, 0], float(os.environ['MASK_VALUE']))
    return np.argmax(np.concatenate(predictions, axis=0), axis=1)[~M.mask.reshape(-1)]


def tn_custom_trajectory_predictor(data, run=0):
    batch_size = int(os.environ['TN_BATCH_SIZE_INFERENCE'])

    x, y = data
    
    name = f"CUSTOM_{run}"
    model = load_model(f"{os.environ['MODEL_DIR']}/tn/{name}.hdf5",
        custom_objects={'Encoder': Encoder,
                        'EncoderLayer': EncoderLayer,
                        'MultiHeadAttention': MultiHeadAttention})
    predictions = model.predict(np.array(x), batch_size=batch_size, verbose=0)

    M = np.ma.masked_equal(np.concatenate(x)[:, 0], float(os.environ['MASK_VALUE']))
    return np.argmax(np.concatenate(predictions, axis=0), axis=1)[~M.mask.reshape(-1)]


def lstm_raw_trajectory_predictor(data, run=0):
    batch_size = int(os.environ['LSTM_BATCH_SIZE_INFERENCE'])

    x, y = data

    name = f"5_{run}"
    model = load_model(f"{os.environ['MODEL_DIR']}/lstm/{name}.hdf5")
    predictions = model.predict(np.array(x), batch_size=batch_size, verbose=0)

    M = np.ma.masked_equal(np.concatenate(x)[:, 0], float(os.environ['MASK_VALUE']))
    return np.argmax(np.concatenate(predictions, axis=0), axis=1)[~M.mask.reshape(-1)]


def tn_raw_trajectory_predictor(data, run=0):
    batch_size = int(os.environ['TN_BATCH_SIZE_INFERENCE'])

    x, y = data

    name = f"5_{run}"
    model = load_model(f"{os.environ['MODEL_DIR']}/tn/{name}.hdf5",
        custom_objects={'Encoder': Encoder,
                        'EncoderLayer': EncoderLayer,
                        'MultiHeadAttention': MultiHeadAttention})
    predictions = model.predict(np.array(x), batch_size=batch_size, verbose=0)

    M = np.ma.masked_equal(np.concatenate(x)[:, 0], float(os.environ['MASK_VALUE']))
    return np.argmax(np.concatenate(predictions, axis=0), axis=1)[~M.mask.reshape(-1)]


def lstm_raw_trajectory_predictor_augmented(data, run=0):
    batch_size = int(os.environ['LSTM_BATCH_SIZE_INFERENCE'])

    x, y = data

    name = f"AUGMENTED_{run}"
    model = load_model(f"{os.environ['MODEL_DIR']}/lstm/{name}.hdf5", compile=False)
    optimizer = "Adam"
    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'])
    predictions = model.predict(np.array(x), batch_size=batch_size, verbose=0)

    M = np.ma.masked_equal(np.concatenate(x)[:, 0], float(os.environ['MASK_VALUE']))
    return np.argmax(np.concatenate(predictions, axis=0), axis=1)[~M.mask.reshape(-1)]


def tn_raw_trajectory_predictor_augmented(data, run=0):
    batch_size = int(os.environ['TN_BATCH_SIZE_INFERENCE'])

    x, y = data

    name = f"AUGMENTED_{run}"
    model = load_model(f"{os.environ['MODEL_DIR']}/tn/{name}.hdf5",
        custom_objects={'Encoder': Encoder,
                        'EncoderLayer': EncoderLayer}, compile=False)
    optimizer = "Adam"
    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'])
    predictions = model.predict(np.array(x), batch_size=batch_size, verbose=0)

    M = np.ma.masked_equal(np.concatenate(x)[:, 0], float(os.environ['MASK_VALUE']))
    return np.argmax(np.concatenate(predictions, axis=0), axis=1)[~M.mask.reshape(-1)]


def dt_predictor(data, run=0):
    _ = run
    x_train, y_train, x_test, y_test, best_params = data
    x_train, y_train = unison_shuffled_copies(x_train, y_train)
    dt = DecisionTreeClassifier(max_depth=best_params['max_depth'], criterion=best_params['criterion'],
                                class_weight='balanced',
                                splitter="best")
    dt.fit(x_train, y_train)

    return dt.predict(x_test)


def rf_predictor(data, run=0):
    _ = run
    x_train, y_train, x_test, y_test, best_params = data
    x_train, y_train = unison_shuffled_copies(x_train, y_train)
    rf = RandomForestClassifier(n_estimators=best_params['n_estimators'], max_depth=best_params['max_depth'], n_jobs=-1)
    rf.fit(x_train, y_train)

    return rf.predict(x_test)


def hmm_raw_predictor(data, run=0):
    x_train, y_train, x_test, y_test = data

    name = f"5_{run}"
    model = pickle.load(open(f"{os.environ['MODEL_DIR']}/hmm/{name}.pkl", 'rb'))
    predictions = model.predict(x_test)

    return np.array(predictions)

def hmm_custom_predictor(data, run=0):
    x_train, y_train, x_test, y_test = data

    name = f"CUSTOM_{run}"
    model = pickle.load(open(f"{os.environ['MODEL_DIR']}/hmm/{name}.pkl", 'rb'))
    predictions = model.predict(x_test)

    return np.array(predictions)