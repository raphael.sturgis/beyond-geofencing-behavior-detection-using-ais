import os
import pickle
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from dotenv import load_dotenv
from sklearn.metrics import accuracy_score, f1_score, confusion_matrix
from tqdm import tqdm

from src.scripts.extract_predictions import prediction_pipelines
from src.utils.utils import get_transition_indexes, get_point_near_transition

np.seterr(divide='ignore', invalid='ignore')


def get_near_point_performances_multi_run(name, windows, runs):
    accs_in = []
    f1s_in = []
    recall_per_class = []
    precision_per_class = []
    trajectories = pickle.load(open(f"{data_dir}/predictions/lax_predictions.pkl", 'rb'))
    transitions = get_transition_indexes(trajectories)

    for window in tqdm(windows, colour="blue", position=1, desc="window", leave=False):
        points, _ = get_point_near_transition(trajectories, window, transitions)
        y_true = points['label'].to_numpy()
        acc_tmp = []
        f1_tmp = []

        recall_per_class_per_run = [[] for _ in range(4)]
        precision_per_class_per_run = [[] for _ in range(4)]
        for run in tqdm(range(runs), colour="green", position=2, desc="run", leave=False):
            y_pred = points[f'prediction_{name}_{run}'].to_numpy()

            acc_tmp.append(accuracy_score(y_true, y_pred))
            f1_tmp.append(f1_score(y_true, y_pred, average='macro'))
            matrix = confusion_matrix(y_true, y_pred)

            precision = matrix.diagonal() / matrix.sum(axis=0)
            recall = matrix.diagonal() / matrix.sum(axis=1)
            for i in range(4):
                recall_per_class_per_run[i].append(recall[i])
                precision_per_class_per_run[i].append(precision[i])

        accs_in.append(np.mean(acc_tmp))
        f1s_in.append(np.mean(f1_tmp))
        tmp_recall_per_class = []
        tmp_precision_per_class = []
        for i in range(4):
            tmp_recall_per_class.append(np.mean(recall_per_class_per_run[i]))
            tmp_precision_per_class.append(np.mean(precision_per_class_per_run[i]))
        recall_per_class.append(tmp_recall_per_class)
        precision_per_class.append(tmp_precision_per_class)
    return accs_in, f1s_in, np.array(recall_per_class), np.array(precision_per_class)


classes = ['Underway', 'Moored', 'At Anchor', 'Drifting']
model_ids = [0, 2, 6, 4, 9, -1]

windows = [1, 5, 10, 20, 30, 40, 50, 60]

if __name__ == '__main__':
    load_dotenv()
    data_dir = os.environ["DATA_DIR"]

    trajectories_LAX = pickle.load(open(f"{data_dir}/predictions/lax_predictions.pkl", 'rb'))
    trajectories_NE = pickle.load(open(f"{data_dir}/predictions/ne_predictions.pkl", 'rb'))
    Path(f"images").mkdir(parents=True, exist_ok=True)

    iterator = tqdm([prediction_pipelines[i] for i in model_ids], colour="magenta", position=0, leave=True)
    data = []
    for name, name_pretty, _, _, runs, _ in iterator:
        iterator.set_description(name)
        accs_in, f1s_in, recall_per_class, precision_per_class = get_near_point_performances_multi_run(name, windows,
                                                                                                       runs=runs)
        data.append((name, name_pretty, runs, accs_in, f1s_in, recall_per_class, precision_per_class))

    fig1, model_acc_comp = plt.subplots()
    fig2, model_f1_comp = plt.subplots()
    for name, name_pretty, runs, accs_in, f1s_in, recall_per_class, precision_per_class in data:
        fig3, model_per_class_precision = plt.subplots()
        fig4, model_per_class_recall = plt.subplots()

        model_acc_comp.plot(windows, accs_in, label=name_pretty)
        model_f1_comp.plot(windows, f1s_in, label=name_pretty)

        for i in range(4):
            model_per_class_precision.plot(windows, precision_per_class[:, i], label=classes[i])
            model_per_class_recall.plot(windows, recall_per_class[:, i], label=classes[i])

        model_per_class_precision.legend()
        model_per_class_precision.set_title(name_pretty)
        fig3.savefig(f"images/{name}_precision_per_class.png")

        model_per_class_recall.legend()
        model_per_class_recall.set_title(name_pretty)
        fig4.savefig(f"images/{name}_recall_per_class.png")
    model_acc_comp.legend()
    model_acc_comp.set_xlabel('Window radius (seconds)')
    model_acc_comp.set_title('Accuracy')
    fig1.savefig(f"images/acc.png")

    model_f1_comp.legend()
    model_f1_comp.set_xlabel('Window radius (seconds)')
    model_f1_comp.set_title('F1 score')
    fig2.savefig(f"mages/f1.png")
