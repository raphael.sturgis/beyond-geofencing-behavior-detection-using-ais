import os
import sys
from pathlib import Path

from dotenv import load_dotenv
from skais.ais.ais_points import AISPoints

from src.models.transformer import create_tn
from src.utils.data_import import load_dataset_from_csv
from src.utils.experiment_tools import normalize_set_of_trajectories, convert_to_vectors, split_to_size_list
from src.utils.utils import shift

if __name__ == '__main__':
    run = int(sys.argv[1])
    load_dotenv()
    Path(f"{os.environ['MODEL_DIR']}/tn/").mkdir(parents=True, exist_ok=True)

    number_of_layers = int(os.environ["TN_RAW_NUMBER_OF_LAYERS"])
    d_model = int(os.environ["TN_RAW_D_MODEL"])
    dff = int(os.environ["TN_RAW_DFF"])
    number_of_heads = int(os.environ["TN_RAW_NUMBER_OF_HEADS"])

    trajectories_train = load_dataset_from_csv(os.environ["NE_DATA"], [int(i) for i in os.environ["NE_TRAIN_INDICES"].split(" ")])

    points = AISPoints.fuse(trajectories_train)
    normalisation_factors = points.normalize(
        third_quartile_features=['sog'],
        divide_by_value=[('latitude', 90), ('longitude', 180), ('heading', 180), ('cog', 180)]
    )
    features = ['sog', 'cog', 'heading', 'longitude', 'latitude']
    normalize_set_of_trajectories(trajectories_train, normalisation_factors)

    shifted = shift(trajectories_train, int(os.environ["TN_AUGMENTED_AUGMENTATION_FACTOR"]))
    x_train, y_train = convert_to_vectors(shifted, features)
    x_train, y_train = split_to_size_list(x_train, y_train, size=int(os.environ["TIME_SERIES_LENGTH"]), value=float(os.environ["MASK_VALUE"]))


    name = f"AUGMENTED_{run}"

    create_tn(x_train, y_train, features, number_of_layers, d_model, dff, number_of_heads, name)